FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append:riscv64 =  " \
    file://0001-sbi-srst-support-reset.patch \
    file://0001-merge-penglai-patch.patch \
    file://0001-disable-ftrace.patch \
    file://0001-increase-pgd-memory.patch \
    file://0001-increase-total-pt-area-page.patch \
    "
