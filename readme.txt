# meta-penglai

## intput 

. ./meta-penglai/setup.sh

MACHINE=unmatched bitbake peng-lai-all

## output 

deploy/images/unmatched/peng-lai-all-unmatched.wic.xz

# enter file system, then run penglai SDK
cd  /lib/modules/5.13.19
insmod penglai.ko

cd  /home/root

#run hello-world
./host hello-world

#run stop
./test-stop loop

#run ipc
./test-caller caller server server1

#run mem
./host mem
 