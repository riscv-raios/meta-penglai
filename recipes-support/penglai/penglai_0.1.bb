# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)


LICENSE = "MulanPSL-1.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=af2f9b5e81211b3b2c24fbf92186b2ef"


SRC_URI = "git://git.mirror.iscas.ac.cn/github.com/Penglai-Enclave/Penglai-sdk-TVM.git;protocol=https;branch=main \
           file://0001-change-function.patch \
           file://0001-change-makefile.patch \ 
           file://0001-change-ko-makefile.patch \
          "
SRCREV = "13370348a2dffcf2c1f1f6ec653af2cb76b800f1"
PV = "0.1+git${SRCPV}"
                                    
S = "${WORKDIR}/git"
inherit module 

export  PENGLAI_SDK= "$S"
do_compile() { 
echo $PENGLAI_SDK
cp  ${WORKDIR}/recipe-sysroot/usr/lib/libc.a   ${WORKDIR}/recipe-sysroot/usr/lib/riscv64-oe-linux/11.2.0/
cd enclave-driver
make
cd ..
make
}

do_install(  ){
  install -d ${D}/lib/modules/${KERNEL_VERSION}
  install -d ${D}/home/root
  cd  ${WORKDIR}/git/enclave-driver
  install -m 0644 penglai.ko ${D}/lib/modules/${KERNEL_VERSION}
  cd   ../demo/host
  install -m 0755   host ${D}/home/root
  cd  ../hello-world
  install -m 0644 hello-world  ${D}/home/root
  cd  ../IPC/test-caller
  install -m 0755   test-caller ${D}/home/root
  cd   ../caller
  install -m 0644 caller  ${D}/home/root
  cd   ../server
  install -m 0644 server  ${D}/home/root
  cd   ../server1
  install -m 0644 server1  ${D}/home/root
  cd  ../../FS/fshost
  install -m 0755   fshost ${D}/home/root
  cd  ../testfs
  install -m 0644   testfs ${D}/home/root
  cd  ../littlefs
  install -m 0644   lfs ${D}/home/root
  cd  ../../test-stop/host
  install -m 0755   test-stop  ${D}/home/root
  cd  ../loop
  install -m 0644    loop  ${D}/home/root
   cd  ../../mem
   install -m 0755   mem  ${D}/home/root
}

FILES:${PN}+= "/home/root* "

